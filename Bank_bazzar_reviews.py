# -*- coding: utf-8 -*-
"""
Created on Mon Jul 20 18:46:43 2020
@author: Anonymous
"""
import pandas as pd 
import requests
from bs4 import BeautifulSoup

# Send Request 
url = 'https://www.bankbazaar.com/reviews.html'
page = requests.get(url)

# Formatting to user readable form 
Soup = BeautifulSoup(page.text,'html.parser')

# reading reviews 
review_text = list()
review_text_elem = Soup.find_all(class_ = "text_here review-desc-more")

for item in review_text_elem:
    review_text.append(item.text)

# read user name 
    
User_name = list()
User_name_elem = Soup.find_all(class_="js-author-name")

for item in User_name_elem:
    User_name.append(item.text)

# Bank name 

Bank_name = list()
Bank_name_elem = Soup.find_all('div',{'class':'review-bank-title'})

for item in Bank_name_elem:
    Bank_name.append(item.find('img').get('alt'))
    
# Create a pandas dataframe 

Final_Array = []

for text,user,bank in zip(review_text,User_name,Bank_name):
    Final_Array.append({'Review':text,'User':user,'Bank':bank})
    
df = pd.DataFrame(Final_Array)
    
df.to_excel("./Final_Array_1.xlsx");