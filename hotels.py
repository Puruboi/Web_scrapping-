# -*- coding: utf-8 -*-
"""
Created on Sat Aug  8 07:35:42 2020

@author: Anonymous
"""
import pandas as pd 
import requests
from bs4 import BeautifulSoup
# API call to scrap page
if __name__ == '__main__':
    API_KEY = '3e4a0c98a9ad190309924fa7cbe8d927'
    URL_TO_SCRAPE = 'https://www.trip.com/hotels/list?city=1&countryId=1&checkin=2020/11/18&checkout=2020/11/25&optionId=1&optionType=City&directSearch=0&display=Beijing&crn=1&adult=2&children=0&searchBoxArg=t&travelPurpose=0&ctm_ref=ix_sb_dl&domestic=1'
payload = {'api_key': API_KEY, 'url': URL_TO_SCRAPE}
r = requests.get('http://api.scraperapi.com', params=payload, timeout=60)
print(r.text)

soup = BeautifulSoup(r.text,'html.parser')

"""# laptop names 
hotel_names = list()
hotel_names_elem = soup.find_all(class_ = "name font-bold")

for item in hotel_names_elem:
    hotel_names.append(item.text)"""
    
def scraping(class_attribute):
    elements= list()
    elem = soup.find_all(class_ = class_attribute)
    for item in elem:
        elements.append(item.text)
    return elements

hotel_names = scraping("name font-bold")

address = scraping("transport")

review = scraping("me")

Final_Array = []
for nam,add,rev in zip(hotel_names,address,review):
    Final_Array.append({'city':'Beijjing',
                        'Check-in-date':'18th Nov 2020',
                        'Night':7, 'adult':2,
                        'Hotel':nam,
                        'User_rating':rev,
                        'address':add})
    
df = pd.DataFrame(Final_Array)
    
df.to_excel("./hotels.xlsx");