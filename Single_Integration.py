# -*- coding: utf-8 -*-
"""
Created on Fri Sep 11 10:32:08 2020

@author: Anonymous
"""

from scipy.integrate import quad
from math import sqrt
def integrand(x, a, b,c,d):
    return (((a*(x**3))-(x**2)+(c*x)-d)/(sqrt((x**2)-(a*x)+c)))

a = 3
b = 1
c = 2
d = 4
I = quad(integrand, 0, 1, args=(a,b,c,d))
I